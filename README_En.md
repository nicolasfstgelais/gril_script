# **GRIL Script Share**

Welcome to the GRIL script share. The main objectives of this repository are to facilitate script sharing between GRIL members and to provide a network of support for using statistical methods in research.

### What is this repository for?

In this repository you will find a number of different statisitcal methods for use in a variety of programs (including R) as well as contact information for a number of fellow GRIL members who may be able to help you with statistical coding problems. Scripts are accessible through BitBucket (and its desktop counterpart SourceTree), and fellow GRIL members can be contacted through HipChat.

* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

## Getting Started

To begin, you need to be able to access BitBucket. Scripts can be downladed from the project without making an account, however, you need to have an account to be able to upload your own scripts, and ask questions about them. SourceTree is a desktop client that allows you to easily upload and download files stored on BitBucket - more easily that through BitBucket alone.

### Setting up BitBucket  

1. Go to [Bitbucket](https://bitbucket.org) and make an account
2. Set up [git] (https://confluence.atlassian.com/bitbucket/set-up-git-744723531.html) on you computer.
3. Install [SourceTree](https://www.sourcetreeapp.com/)
> Differences for Mac users?

Once you have set up your BitBucket account, and installed SourceTree, you have to clone the BitBucket GRIL Script Share repository to your desktop. If at any time you are confused about how BitBucket works, try looking at the tutorials the BitBucket developers have created.

### Clone a repository

Cloning the repository is relativly painless, if you get stuck, try working through the BitBucket tutorials.

1. Navigate to the repository in Bitbucket
2. Click the **Clone** button
3. Click **Clone in Source Tree**
4. Click **Launch Application**
5. In Source Tree select a Destination Path
6. In Source Click **Clone**

Now that the respository is cloned to your SourceTree, you are ready to start adding and downloading scripts.

### How to Add Files/ Push Updates to a Repository Using SourceTree

1. Save your file in the local BitBucket folder
2. In the **Unstaged files** section, click the box next to the file name
3. Click the **Commit** button on the top menu
4. Describe what you changed using the following format:
    * [filename] (ex. [RDA.r])
    * On the same line a short description of what you changed (ex. I added a plot)
    * And a long description or justification, if needed, on the following line
5. Click **Commit**
6. Click the **Push** button on the top menu
7. Click **ok**

### How to Edit a File Using BitBucket

1. Open BitBucket, and navigate to the file.
2. Click on Edit, located on the top right hand corner of the file
3. Make any changes you want.
4. Save changes by clicking commit, on the bottom right of the file.

### How to Download a Script
1. SourceTree: Once you have cloned the BitBucket repository to your computer, you can access these files through your desktop.
2. BitBucket: If you have decided not to clone the respoitory to your computer, you can access these files through BitBucket. Navigate to the file you want using the source tab in the left hand menu, and download the appropriate files.

## Help! I Have a Problem

Now that you have access to the GRIL Script Share respository, and know how to download scripts, you are ready to start your stats. However, you might run into a problem using the code with your own data. We have set up two ways to help you solve problems with your code.

**First:** we have a HipChat room set up. HipChat is linked with BitBucket, and is a online chat room where you can ask questions about any problems you might have.

To use HipChat, you have to create a HipChat [account](https://www.hipchat.com/invite/557670/19fd6dfb681a58ea11742fc82192bcdd?utm_campaign=company_room_link), or log in with you Atlassian credentials. 

**Second:** you can report issues that you may have found within the scripts.

You should report an issue in the following situations:

* There is a bug in a script
* You have a question about a script
* You have a specific request

To report an issue:

1. Click on **Issues** in the menu
2. Click on **Create issue** (top right)
3. Select a title and describe the issue
4. If needed to can assign an issue to a specific user
5. Select the kinf of issue
6. Click on **Create issue**

## Contribution Guidelines ##

As the GRIL is a large community, many members may have scripts for methods that are not already included in the respository. We welcome contributions for:

* Writing tests
* Code review
* Other guidelines