## GRIL script!!!!

-----

####L'objectif de ce répertoire est de favoriser le partage de scripts et la collaboration entre les membres du GRIL.
(For the english version of this README select the README_En.md file) 

Si vous désirez utiliser les scripts du GRIL vous pouvez:

1. Suivre ce [lien](https://bitbucket.org/GRIL_limno/gril_script/downloads)
2. Puis cliquer sur **Dowload repository** et vous aurez accčs ŕ la version la plus récente des scripts GRIL.

Cependant, pour pouvoir maintenir vos scripts ŕ jour ou encore pour participer au développement des scripts GRIL, vous devez configurer [BitBucket](https://bitbucket.org/) sur votre ordinateur. BitBucket est un service d'hébergement pour logiciels de gestion de versions décentralisé  (comme git et mercurial). Avec **git** il est possible de suivre l'évolution d'un document dans le temps. Pour en apprendre plus sur **git** et le fonctionnement de BitBUcket, voici un excellent [tutoriel](https://www.atlassian.com/git/tutorials/what-is-version-control) (En anglais). Pour configurer git et bitbucket sur votre ordinateur voici les instructions:



#### Installer BitBucket ####

1. Aller ŕ [Bitbucket](https://bitbucket.org) et créer un compte. 
2. Installer [git](https://confluence.atlassian.com/bitbucket/set-up-git-744723531.html) sur votre ordinateur.
3. Installer [SourceTree](https://www.sourcetreeapp.com/) sur votre ordinateur. 

#### Clone le répertoire GRIL script 

1. Aller sur la page de [GRIL script](https://bitbucket.org/GRIL_limno/gril_script/overview)
2. Cliquer sur **Clone** dans le menu de droite
3. Cliquer **Clone in Source Tree**
4. Cliquer **Launch Application**
5. Dans Source Tree sélectionner un dossier pour installer GRIL script
6. Dans Source Tree Cliquer sur **Clone**

#### Ajouter/ modifier un ficher de GRIL script

1. Sauver le fichier dans votre dossier BitBucket local
2. Dans la section **Unstaged files** , cliquer la boite ŕ côté du fichier que vous avez modifié
3. Cliquer sur **Commit** dans le menu du haut
4. Décrire ce que vous avez changé en utilisant le format suivant: 
    * [nomdufichier] (ex. [RDA.r])
    * Sur la męme ligne inclure une courte description de ce que vous avez changé (ex. J'ai ajouté une figure)
    * Et une longue description/justification sur les lignes suivantes
5. Cliquer sur **Commit** dans la fenętre
6. Cliquer sur **Push** dans le menu du haut
7. Cliquer sur **ok**

----

#### Pour accéder au forum de discution de GRIL script

1. Créer un compte [HipChat](https://www.hipchat.com/invite/557670/19fd6dfb681a58ea11742fc82192bcdd?utm_campaign=company_room_link)

#### Pour signaler un problčme

Vous devriez signaler un problčme dans les situations suivantes:
* Il a a un bug dans un script
* Vous avez une question ŕ propos d'un script
* Vous avez une demande 


Pour signaler un problčme:

1. Cliquer sur **Issues** dans le menu de droite
2. Cliquer sur **Create issue** (en haut ŕ droite)
3. Écrire un titre et décrire le problčme en détails
4. Si vous voulez vous pouvez assigner le problčme ŕ un utilisateur en particulier 
5. Sélectionner le type de problčme
6. Cliquer sur **Create issue**
 
 Resources:
-a